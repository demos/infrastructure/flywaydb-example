# FlywayDB Example

Ever have the need to version-control your database and employ the likes of CI/CD to deploy changes? GitLab and FlywayDB have you covered!  This project is a small example of how you can use GitLab with FlywayDB.

Flyway looks great... but why don't I just do this on my own?

* Creates a table in the DB that records what migrations has been applied (think Commit History but on the DB side)
* Provides a structured naming convention for SQL files that allows the script to know how to migrate
* Think about applying AND unapplying schemas (rollback when things break)
* CLIs are built to do dry runs and automatically build the migration DDL files.
* These tools are typically DB Agnostic! Oftentimes you can build this out so that you can use a MySQL DB locally then rely on an Oracle DB in the real world.


Other alternatives.

* https://flywaydb.org/
* https://www.liquibase.org/
* https://sqitch.org/

* Products like Django (which is a Python Web Framework) have these built into its ORM - although some people really dislike ORMs.
